"
" In case of any errors at the startup, try:
" sudo apt-get install vim-gui-common
" sudo apt-get install vim-runtime
"

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" From: 
"       http://amix.dk/vim/vimrc.html
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sets how many lines of history VIM has to remember
set history=700

" Enable filetype plugins
filetype plugin on
filetype indent on

" Set to auto read when a file is changed from the outside
set autoread

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","
let g:mapleader = ","

" Fast saving
nmap <leader>w :w!<cr>

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile

" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
set cindent
set wrap "Wrap lines

" Always show the status line
set laststatus=2

" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l/%L

" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Mouse:
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Fix insertion problem

"let &t_SI .= "\<Esc>[?2004h"
"let &t_EI .= "\<Esc>[?2004l"
"
"inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()
"
"function! XTermPasteBegin()
"  set pastetoggle=<Esc>[201~
"  set paste
"  return ""
"endfunction

set pastetoggle=<Esc>[201~
noremap <Esc>[200~ :set paste<cr>0i
inoremap <Esc>[200~ <c-o>:set paste<cr>
cnoremap <special> <Esc>[200~ <nop>
cnoremap <special> <Esc>[201~ <nop>

let &t_ti.="\<esc>[?2004h"
let &t_te.="\<esc>[?2004l"

" Enable mouse support in console
" TODO: fix mouse left drag and enable mouse
"set mouse=n
"noremap <LeftDrag> <LeftMouse>
"noremap! <LeftDrag> <LeftMouse>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" From: 
"       Different sources
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible
set background=dark
colorscheme Mustang

" Highlight current line
set cursorline

" Disable backuping
set nobackup
set noswapfile

" Disable replace mode
imap <Insert> <Nop>

" Exit from command and insertion mode
inoremap <C-\> <Esc>

" Exit and save in insertion mode
inoremap <Esc>w <Esc>:w!<cr>li
noremap  <Esc>w <Esc>:w!<cr>l

" Remap semicolon
noremap ; :

" This shows what you are typing as a command
set showcmd

" Show current mode
set showmode

" Don't make it look like there are line breaks where there aren't
set nowrap

" Visual options
set number
set ruler
if has("syntax")
  syntax on
endif

" Highlight things that we find with the search
set hlsearch

" Show matching brackets
set showmatch

" Tags
set tags=./tags

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ESCAPE mapping
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Turn off Paste mode
" Turn highlight off in normal mode
map <silent> <Esc> :set nopaste \| nohlsearch<cr>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Windows
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Multiple windows, when created, are equal in size
set equalalways       
" Put the new windows to the right/bottom    
set splitbelow splitright 
set winminheight=0
set winminwidth=0

" let &winheight = &lines * 7 / 10 
let &winheight = ( &winheight < 3 ? ( &lines * 3 / 10 ) : &winheight )

" Hit C-j to move the current split down and maximize it
map <C-J> <C-W>j<C-W>_ 

" Hit C-k to move the current split up and maximize it
map <C-K> <C-W>k<C-W>_ 

" Hit C-l to move the current split right and maximize it
map <C-L> <C-W>l:vertical res<cr>

" Hit C-h to move the current split left and maximize it
map <C-H> <C-W>h:vertical res<cr>

" Smart way to move between windows
" Ctrl+ArrowUp
map <ESC>[1;5A <C-W>k
inoremap <ESC>[1;5A <ESC><C-W>k
" Ctrl+ArrowDown
map <ESC>[1;5B <C-W>j
inoremap <ESC>[1;5B <ESC><C-W>j
" Ctrl+ArrowRight
map <ESC>[1;5C <C-W>l
inoremap <ESC>[1;5C <ESC><C-W>l
" Ctrl+ArrowLeft
map <ESC>[1;5D <C-W>h
inoremap <ESC>[1;5D <ESC><C-W>h

" Window resizing
map <F5> <C-W>+
map <F6> <C-W>-
map <F7> <C-W><
map <F8> <C-W>>

" Full size
map <F3> <ESC>:vertical resize<cr>:resize<cr>
" Equal size
map <F10> <ESC><C-w>=

" Save session in tmp.vim
nmap <ESC>p :mksession! ./tmp.vim<cr>
nmap <ESC>g :source ./tmp.vim<cr>:noh<cr>

" Allows all window commands in insert mode and i'm not accidentally deleting words anymore
" :imap <C-w> <C-o><C-w>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Edition
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remove next word (like in shell)
" try also <C-[>diwi or <C-o>diw
inoremap <ESC>d <ESC>lcw
inoremap <ESC>k <ESC>lC

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" C options
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" This is the way I like my quotation marks and various braces
inoremap '' ''<Left>
inoremap "" ""<Left>
inoremap () ()<Left>
inoremap <> <><Left>
inoremap {} {}<Left>
inoremap [] []<Left>
inoremap () ()<Left>

" Usefull snippets for c programming
inoremap def<Tab> #define 
inoremap nc<Tab> #include <><Left>
inoremap inc<Tab> #include ""<Left>
inoremap pr<Tab> printf("---> ", );
inoremap main<Tab> int main(int argc, char **argv)<Enter>{<Enter>return 0;<Enter>}<Up><Up><Up>

" Fold code block
map - [{v%zf

" Put -> for structure member
inoremap <Esc>. ->

map <ESC>/ ^i//<Esc>
imap <ESC>/ <Esc>^i//<Esc>
" vmap <ESC> "cdi/**/<Esc>h"cP

nmap <ESC>c :!g++ -o main main.c*<cr>
nmap <ESC>r :!./main<cr>
nmap <ESC>m :!make<cr>

" Enable doxygen highlighting
let g:load_doxygen_syntax=1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins setup
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Nerdtree setup
map <F4> :NERDTreeToggle<CR>
imap <F4> <Esc>:NERDTreeToggle<CR>
vmap <F4> <Esc>:NERDTreeToggle<CR>

" Taglist setup
map <F2> :TlistToggle<CR>
imap <F2> <Esc>:TlistToggle<CR>
vmap <F2> <Esc>:TlistToggle<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Trailing whitespaces
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" match ErrorMsg '\s\+$'

" Removes trailing spaces
function! TrimWhiteSpace()
    %s/\s\+$//e
endfunction

" autocmd FileWritePre    * :call TrimWhiteSpace()
" autocmd FileAppendPre   * :call TrimWhiteSpace()
" autocmd FilterWritePre  * :call TrimWhiteSpace()
" autocmd BufWritePre     * :call TrimWhiteSpace()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" From: 
"       LinuxJournal
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Setup search options
set incsearch
set ignorecase
set nowrapscan
set smartcase
set scrolloff=2

" Cool tab completion stuff
set wildmenu
set wildmode=list:longest,full

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" From:
" http://blog.wuwon.id.au/2011/10/vim-plugin-for-navigating-c-with.html
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
execute pathogen#infect()

let g:clang_auto_select=1
let g:clang_complete_auto=0
let g:clang_complete_copen=1
let g:clang_hl_errors=1
let g:clang_periodic_quickfix=0
let g:clang_snippets=1
let g:clang_snippets_engine="clang_complete"
let g:clang_conceal_snippets=1
let g:clang_exec="clang"
let g:clang_user_options=""
let g:clang_auto_user_options="path, .clang_complete"
let g:clang_use_library=1
" Output from llvm-config --libdir
let g:clang_library_path="/usr/lib/llvm-3.4/lib"
let g:clang_sort_algo="priority"
let g:clang_complete_macros=1
let g:clang_complete_patterns=0
nnoremap <Leader>q :call g:ClangUpdateQuickFix()<CR>
"let g:clic_filename=$HOME."/devel/dtcp_plus/new_rep/index_db/index.db"
let g:clic_filename=getcwd()."/../indexer_db/index.db"
nnoremap <Leader>r :call ClangGetReferences()<CR>
nnoremap <Leader>d :call ClangGetDeclarations()<CR>
nnoremap <Leader>s :call ClangGetSubclasses()<CR>


